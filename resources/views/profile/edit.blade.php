@extends('app')

@section('content')
    <a href="/auth/logout" class="btn">&laquo; Logout</a>
    <form method="POST" action="/profile/edit" class="app-single-form">
        {!! csrf_field() !!}

        @if (Session::has('message'))
            <div class="alert alert-success">{!! Session::get('message') !!}</div>
        @endif

        <p>Your email is <strong>{{ $email }}</strong></p>

        <input type="hidden" name="email" value="{{ $email }}">

        <div class="form-group {{ count($errors) ? 'has-error' : '' }}">
            <label for="name" class="sr-only control-label">Name</label>
            <input type="text" id="name" name="name" value="{{ $name }}" class="form-control" placeholder="Your name">
            {!! $errors->first('name', '<span class="error">:message</span>') !!}
        </div>

        <div>
            <button class="btn btn-primary btn-block" type="submit">Update profile</button>
        </div>
    </form>
@stop