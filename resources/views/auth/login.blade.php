@extends('app')

@section('content')
<form method="POST" action="/auth/password" class="app-single-form">
    {!! csrf_field() !!}
    <h1>Please login</h1>
    <div class="form-group {{ count($errors) ? 'has-error' : '' }}">
        <label for="email" class="sr-only control-label">Email</label>
        <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email address">
        {!! $errors->first('email', '<span class="error">:message</span>') !!}
    </div>

    <div>
        <button class="btn btn-primary btn-block" type="submit">Request password</button>
    </div>
</form>
@stop