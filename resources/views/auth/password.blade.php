@extends('app')

@section('content')
<form method="POST" action="/auth/login" class="app-single-form">
    {!! csrf_field() !!}

    <p>Provide password for <strong>{{ $email }}</strong></p>

    <input type="hidden" name="email" value="{{ $email }}">

    <div class="form-group">
        <label for="password" class="sr-only">Password</label>
        <input type="password" name="password" id="password" placeholder="password" class="form-control">
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" name="remember"> Remember Me
        </label>
    </div>

    <div>
        <button class="btn btn-primary btn-block" type="submit">Login</button>
    </div>
</form>
@stop