<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests\EditProfileRequest;

class ProfileController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Profile Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for editing User profile information
    |
    */

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getEdit(){
        $user = Auth::User();
        return view('profile.edit')->with([
            'email'=> $user->email,
            'name' => $user->name
        ]);
    }

    public function postEdit(EditProfileRequest $request){
        $user = Auth::User();
        $user->name = $request->name;
        $user->save();
        \Session::flash('message', 'Successfully updated!');
        return redirect('/profile/edit');
    }
}
