<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    protected $redirectTo = '/profile/edit';

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }


    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return view('auth.login');
    }

    public function postPassword(Request $request){
        $email = $request->get('email');

        $this->validate($request, [
            'email' => 'required|email|max:255'
        ]);

        $password = $this->generatePasswordForUser($email);

        Mail::send('emails.password', compact('email', 'password'), function($message) use ($email)
        {
            $message->to($email)->subject('Your new password!');
        });

        return redirect('/auth/password?email='.$email);
    }

    public function getPassword(Request $request){
        return view('auth.password', $request->only('email'));
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request){
        $this->validate($request, [
            'email' => 'required', 'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, $request->has('remember'))) {
            return redirect()->intended($this->redirectTo);
        }

        return redirect('/auth/login')
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => 'These credentials do not match our records.',
            ]);
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        Auth::logout();

        return redirect('/auth/login');
    }

    /**
     * Return password for new or existing user with this email
     * @param string $email
     * @return string
     */
    protected function generatePasswordForUser($email){
        $user = User::firstOrNew(['email' => $email]);
        $password = substr(md5(microtime()),0,4);
        $user->password = bcrypt($password);
        $user->save();
        return $password;
    }
}
